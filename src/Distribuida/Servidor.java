/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Distribuida;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import pecl3.dependiente.HerramientasDependiente;

/**
 *
 * @author peta_zetas
 */
public class Servidor extends Thread{
    
    private ServerSocket server;
    private HerramientasDependiente herramientas, caja, caja2, carnicero, pescadero;

    public Servidor(HerramientasDependiente herramientas, HerramientasDependiente caja, HerramientasDependiente caja2, HerramientasDependiente carnicero, HerramientasDependiente pescadero) {
        this.herramientas = herramientas;
        this.caja = caja;
        this.caja2 = caja2;
        this.carnicero = carnicero;
        this.pescadero = pescadero;
        try {
            server = new ServerSocket(5000);
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void responde() throws IOException{
        
        Socket socket;
        DataOutputStream salida;
        DataInputStream entrada;    
        Socket socket2 = server.accept();                                               //Escucha para una conexion
        
        Tarea tarea = new Tarea(herramientas, socket2, caja, caja2, carnicero, pescadero);
        tarea.start();
    }
    public void run() {
        while(true){
            try {
                responde();
            } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        
    }
    
}
