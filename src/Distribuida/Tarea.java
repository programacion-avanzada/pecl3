/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Distribuida;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import pecl3.dependiente.HerramientasDependiente;

/**
 *
 * @author peta_zetas
 */
public class Tarea extends Thread{
    
    private Socket socket;
    private HerramientasDependiente herramientas, caja, caja2, carnicero, pescadero;

    public Tarea(HerramientasDependiente herramientas, Socket socket, HerramientasDependiente caja, HerramientasDependiente caja2, HerramientasDependiente carnicero, HerramientasDependiente pescadero) {
        this.herramientas = herramientas;
        this.caja = caja;
        this.caja2 = caja2;
        this.carnicero = carnicero;
        this.pescadero = pescadero;
        this.socket = socket;
    }

    public void run(){
        
        DataOutputStream salida;
        DataInputStream entrada;
                   
        while(true){
            
            try{
                
                //socket = server.accept();                                   //Esperamos la conexión
                entrada = new DataInputStream(socket.getInputStream());     //Abrimos canales Entrada
                salida = new DataOutputStream(socket.getOutputStream());    //Abrimos canales Salida
                
                String mensaje = entrada.readUTF();                         //Leemos el mensaje del cliente
                System.out.println(mensaje);
                
                if(mensaje.equals("Reanudar Mercado")){    
                    herramientas.reanudar();
                    caja.reanudar();
                    caja2.reanudar();
                    carnicero.reanudar();
                    pescadero.reanudar();
                }
                
                else if(mensaje.equals("Reanudar Pescadero")){
                    pescadero.reanudarPescadero();
                }
                
                else if(mensaje.equals("Reanudar Carnicero")){
                    pescadero.reanudarCarnicero();
                }
                
                else if(mensaje.equals("Parar Mercado")){
                    herramientas.parar();
                    caja.parar();
                    caja2.parar();
                    carnicero.parar();
                    pescadero.parar();
                }
                
                else if(mensaje.equals("Parar Pescadero")){
                    pescadero.pararPescadero();
                }
                
                else if(mensaje.equals("Parar Carnicero")){
                    carnicero.pararCarnicero();
                }
                
                //socket.close();                                             //Se cierra la conexion
            }catch(IOException e){}
        }        
    }
    
}

