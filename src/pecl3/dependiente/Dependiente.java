/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pecl3.dependiente;

/**
 *
 * @author peta_zetas
 */
public class Dependiente extends Thread{
    private HerramientasDependiente herramientasDependiente;

    public Dependiente(HerramientasDependiente herramientasDependiente) {
        this.herramientasDependiente = herramientasDependiente;
    }
    
    public void run(){
        herramientasDependiente.atender();   
    }
}
