/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pecl3.dependiente;

/**
 *
 * @author peta_zetas
 */
public class DependienteCaja extends Thread{
    
    private HerramientasDependiente herramientasDependiente;

    public DependienteCaja(HerramientasDependiente herramientasDependiente) {
        this.herramientasDependiente = herramientasDependiente;
    }
    
    public void run(){
        herramientasDependiente.atenderCaja();
    }


}
