/*
 * 
 *
 */
package pecl3.dependiente;

import dispatcher.HerramientasDispatcher;
import java.util.ArrayList;
import java.util.Date;
import pecl3.comprador.Comprador;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import log.Log;
import pecl3.Cola.Cola;
import pecl3.Estantes.Estante;
import pecl3.comprador.Calculador;
import pecl3.comprador.HerramientasGenerador;

/**
 *
 * @author peta_zetas
 */
public class HerramientasDependiente{
    
    private String tipoVendedor;
    private Cola colaVendedor, colaCaja;
    private HerramientasDispatcher lugarCompra;
    private JTextField siendoAtendido;
    private boolean parado, finalizado, paradoPescadero, paradoCarnicero;
    private Log log;
    private Estante estante;
    private ArrayList<Float> tiemposPescadero, tiemposCarnicero;  
    private Calculador calculador;
    //Constructor
    public HerramientasDependiente(String tipoVendedor, Cola colaVendedor, HerramientasDispatcher lugarCompra, Cola colaCaja, JTextField siendoAtendido, Log log, Calculador calculador) {
        this.tipoVendedor = tipoVendedor;
        this.colaVendedor = colaVendedor;
        this.lugarCompra = lugarCompra;
        this.colaCaja = colaCaja;
        this.siendoAtendido = siendoAtendido;
        this.parado = false;
        this.finalizado = false;
        this.log = log;
        this.tiemposCarnicero = new ArrayList<>();
        this.tiemposPescadero = new ArrayList<>();
        this.calculador = calculador;

    }    
    
    //Atiende (pierde tiempo) 
    public void atender(){
      
        while(true){
            
            monitor();
            
            int tiempoAtencion;
            Random rand = new Random();
            
            long ini = (new Date()).getTime();
            
            if(!(colaVendedor.getCola().isEmpty())){
                
                Comprador comprador = colaVendedor.getPrimero();
            
                //System.out.println(comprador.getIdentificador());

                if(tipoVendedor=="Pescadero"){
                    monitorPescadero();
                    tiempoAtencion = rand.nextInt(1000)+2000;                                  
                    imprimir(comprador);                                                        //Se imprime el comprador en el cuadro de texto
                    log.enviaMensaje("Pescadero está atendiendo a Comprador "+comprador.getIdentificador());
                    try {
                        TimeUnit.MILLISECONDS.sleep(tiempoAtencion);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(HerramientasDependiente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    colaCaja.meterACola(comprador);                                             //Se mete en la cola de la caja
                    log.enviaMensaje("Pescadero ha atendido a Comprador "+comprador.getIdentificador());
                    log.enviaMensaje("El comprador "+ comprador.getIdentificador()+" está en la cola de la caja");

                    siendoAtendido.setText("");                                                 //Se quita del cuadro de texto
                    
                    long fin = (new Date()).getTime();
                    
                    float duracion = (float)(fin - ini)/1000;
                    
                    log.enviaMensaje("Duracion de comprador " + comprador.getIdentificador() + " en " + tipoVendedor + " es de "+Float.toString(duracion)+ "segundos");
                    tiemposPescadero.add(duracion);
                    
                    float suma = 0;
                    for (int i = 0; i < tiemposPescadero.size(); i++) {
                        suma = suma + tiemposPescadero.get(i);                        
                    }
                    float media = (float) suma / (float) tiemposPescadero.size();
                    log.enviaMensaje("Duración media de atención en " + tipoVendedor + " es de "+ Float.toString(media));
                    log.enviaMensaje("El " + tipoVendedor + " lleva trabajando "+ suma + " segundos");
                }

                else if (tipoVendedor=="Carnicero"){
                    monitorCarnicero();
                    tiempoAtencion = rand.nextInt(1000)+1500;                                   //1000 + 1500
                    imprimir(comprador);
                    log.enviaMensaje("Carnicero está atendiendo a Comprador "+comprador.getIdentificador());
                    try {
                        TimeUnit.MILLISECONDS.sleep(tiempoAtencion);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(HerramientasDependiente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    colaCaja.meterACola(comprador);                                             //Se mete en la cola de la caja
                    log.enviaMensaje("Carnicero ha atendido a Comprador "+comprador.getIdentificador());
                    log.enviaMensaje("El comprador "+ comprador.getIdentificador()+" está en la cola de la caja");
                    siendoAtendido.setText("");
                    
                    long fin = (new Date()).getTime();
                    
                    float duracion = (float)(fin - ini)/1000;
                    
                    log.enviaMensaje("Duracion de comprador " + comprador.getIdentificador() + " en " + tipoVendedor + " es de "+Float.toString(duracion)+ " segundos ");
                    tiemposCarnicero.add(duracion);
                    
                    float suma = 0;
                    for (int i = 0; i < tiemposCarnicero.size(); i++) {
                        suma = suma + tiemposCarnicero.get(i);                        
                    }
                    float media = (float) suma / (float) tiemposCarnicero.size();
                    log.enviaMensaje("Duración media de atención en " + tipoVendedor + " es de "+ Float.toString(media));
                    log.enviaMensaje("El " + tipoVendedor + " lleva trabajando "+ suma + " segundos");
                }
 
            }
            else{
                //System.out.println("Nadie en la cola de "+ tipoVendedor);
            }
            
        }
    }
    
    public synchronized void atenderCaja(){
        
        while(true){
            
            monitor();
            
            int tiempoAtencion;
            Random rand = new Random();
            
            try {
                
                Comprador comprador = colaVendedor.getPrimero();
            
                if(tipoVendedor == "Caja"){
                    
                    log.enviaMensaje("Comprador "+comprador.getIdentificador()+" está pagando en la caja");
                    tiempoAtencion = rand.nextInt(2000)+1000;
                    imprimir(comprador);
                    try {
                        TimeUnit.MILLISECONDS.sleep(tiempoAtencion);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(HerramientasDependiente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    lugarCompra.adios();                                                        //Libera un permiso del semáforo, para que entre otro comprador -> no se asigna a otra cola
                    log.enviaMensaje("Comprador "+comprador.getIdentificador()+" ha pagado");
                    log.enviaMensaje("El comprador "+ comprador.getIdentificador()+" se ha marchado del supermercado");
                    siendoAtendido.setText("");
                    calculador.saleUno();
                    log.enviaMensaje("Han salido " + calculador.getNumeroSalientes() + " compradores");
                    
                    long tiempoSalida = (new Date()).getTime();
                    comprador.setTiempoSalida(tiempoSalida);
                    calculador.anadirTiempo(comprador.calcularTiempoDesdeEntrada());
                    calculador.anadirTiempoTotal(comprador.calcularTiempoTotal());
                    log.enviaMensaje("La media dentro del super es de "+ Float.toString(calculador.calcularMedia()));
                    System.out.println("La media dentro del super es de "+ Float.toString(calculador.calcularMedia()));
                    log.enviaMensaje("La media es de "+ Float.toString(calculador.calcularMediaTotal()));
                    
                }
                
                
                log.enviaMensaje("Comprador "+comprador.getIdentificador()+" ha estado dentro del supermercado "  );
            } catch (Exception e) {
            }
        }
    }
    
    
    public void imprimir(Comprador comprador){
        siendoAtendido.setText(String.valueOf(comprador.getIdentificador()));
    }
    
    public synchronized void monitor(){
        
        while(parado || finalizado){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(HerramientasGenerador.class.getName()).log(Level.SEVERE, null, ex);   //

            }
        }
    }
    
    public synchronized void monitorPescadero(){
        
        while(paradoPescadero){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(HerramientasGenerador.class.getName()).log(Level.SEVERE, null, ex);   //

            }
        }
    }
    
    public synchronized void monitorCarnicero(){
        
        while(paradoCarnicero){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(HerramientasGenerador.class.getName()).log(Level.SEVERE, null, ex);   //

            }
        }
    }
    
    public void parar(){
        parado=true;
        System.out.println("Para dependiente!");
    }
    public void pararPescadero(){
        paradoPescadero=true;
    }
      public void pararCarnicero(){
        paradoCarnicero=true;
    }
   
    public synchronized void reanudar(){
        parado = false;
        notifyAll();
        System.out.println("Reanudar Dependiente");
    }
    
    public synchronized void reanudarCarnicero(){
        paradoCarnicero = false;
        notifyAll();
        System.out.println("Reanudar Dependiente");
    }
    
    public synchronized void reanudarPescadero(){
        paradoPescadero = false;
        notifyAll();
        System.out.println("Reanudar Dependiente");
    }
    
     public void finalizar(){
        finalizado=true;
        System.out.println("Para dependiente!");
    }
    
}
