/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pecl3.comprador;

/**
 *
 * @author peta_zetas
 */
public class Generador extends Thread{
    
    private HerramientasGenerador generador;

    public Generador(HerramientasGenerador generador) {
        this.generador = generador;
    }

    public void run(){
        generador.generarCompradores();
    }
    
}
