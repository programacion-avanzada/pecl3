package pecl3.comprador;


import static java.lang.Math.E;
import static java.lang.Thread.sleep;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import log.Log;
import pecl3.Cola.Cola;
import pecl3.Estantes.Estante;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author peta_zetas
 */
public class HerramientasGenerador { 
    
    private Cola colaFuera;
    private int numeroCompradores;
    private int numeroGenerados;
    private JTextField text;
    private boolean parado;
    private boolean finalizado;
    private Log log;
    private Cola colaCaja;
    private Estante estante;

    public HerramientasGenerador(Cola colaFuera, int numeroCompradores, JTextField text, Log log, Cola colaCaja, Estante estante) {
        this.colaFuera = colaFuera;
        this.numeroCompradores = numeroCompradores;
        this.text = text;
        this.parado = false;
        this.finalizado = false;
        this.log=log;
        this.colaCaja = colaCaja;
        this.estante = estante;
    }
    
    public void generarCompradores(){
        
        Random rand = new Random();
        
        for (int i = 1; i <= numeroCompradores; i++) {
            monitor();
            
            numeroGenerados = i;
            imprimir();
            
            Comprador comprador = new Comprador(i, colaCaja, log, estante, null);
            colaFuera.meterACola(comprador);
            log.enviaMensaje("Comprador "+i+" en la cola de fuera del mercado");
            int espera = rand.nextInt(801)+800;                                 //800 + 200
            long momentoCreacion = (new Date()).getTime();
            
            try {
                sleep(espera);
            } catch (InterruptedException ex) {
                Logger.getLogger(HerramientasGenerador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }      
    }
    
    public void imprimir(){
        text.setText(String.valueOf(numeroGenerados));
    }
     
    public synchronized void monitor(){
        
        while(parado || finalizado){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(HerramientasGenerador.class.getName()).log(Level.SEVERE, null, ex);   //

            }
        }
    }
    
    public void parar(){
        parado=true;
        log.enviaMensaje("Generador parado");
    }
    
    public synchronized void reanudar(){
        parado = false;
        notifyAll();
        log.enviaMensaje("Generador reanudado");
    }
    
    public void finalizar(){
        finalizado = true;
        log.enviaMensaje("Finalizar programa");
    }
}
