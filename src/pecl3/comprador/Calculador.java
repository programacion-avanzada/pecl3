/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pecl3.comprador;

import java.util.ArrayList;

/**
 *
 * @author peta_zetas
 */
public class Calculador {
    private int numeroSalientes;
    private int numeroEntrantes;
    private ArrayList<Float> tiempos;
    private ArrayList<Float> tiemposTotales;

    public Calculador(int numeroSalientes, int numeroEntrantes) {
        this.numeroSalientes = numeroSalientes;
        this.numeroEntrantes = numeroEntrantes;
    }
    
    
    public synchronized void saleUno(){
        numeroSalientes ++;
    }
    
    public synchronized void entraUno(){
        numeroEntrantes ++;
    }

    public synchronized int getNumeroSalientes() {
        return numeroSalientes;
    }

    public int getNumeroEntrantes() {
        return numeroEntrantes;
    }
    
    public synchronized void anadirTiempo(float tiempo){
        tiempos.add(tiempo);
    }
    
    public synchronized float calcularMedia(){
        float suma = 0;
        for (int i = 0; i < tiempos.size(); i++) {
            suma = suma + tiempos.get(i);
        }
        suma = suma / (float) tiempos.size();
        return suma;
    }
    
    public synchronized void anadirTiempoTotal(float tiempo){
        tiemposTotales.add(tiempo);
    }
    
    public synchronized float calcularMediaTotal(){
        float suma = 0;
        for (int i = 0; i < tiemposTotales.size(); i++) {
            suma = suma + tiemposTotales.get(i);
        }
        suma = suma / (float) tiemposTotales.size();
        return suma;
    }
 
}
