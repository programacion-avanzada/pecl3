/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pecl3.comprador;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import log.Log;
import pecl3.Cola.Cola;
import pecl3.Estantes.Estante;
import pecl3.dependiente.HerramientasDependiente;

/**
 *
 * @author peta_zetas
 */
public class Comprador extends Thread{
    
    private int identificador;
    private Cola colaCaja;
    private Log log;
    private Estante estante; 
    private long tiempoInicio, tiempoEntrada, tiempoSalida;
    
    public Comprador(int identificador, Cola colaCaja, Log log, Estante estante, Calculador calculador) {
        this.identificador = identificador;
        this.colaCaja = colaCaja;
        this.log = log;
        this.estante = estante;
        this.tiempoInicio = 0;
        this.tiempoEntrada = 0;
        this.tiempoSalida = 0;
    }

    public long getTiempoInicio() {
        return tiempoInicio;
    }

    public void setTiempoInicio(long tiempoInicio) {
        this.tiempoInicio = tiempoInicio;
    }

    public long getTiempoEntrada() {
        return tiempoEntrada;
    }

    public void setTiempoEntrada(long tiempoEntrada) {
        this.tiempoEntrada = tiempoEntrada;
    }

    public long getTiempoSalida() {
        return tiempoSalida;
    }

    public void setTiempoSalida(long tiempoSalida) {
        this.tiempoSalida = tiempoSalida;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    
    public void irAEstante(){
        Random rand = new Random();

        int tiempoAtencion = rand.nextInt(11000)+1000;  
        log.enviaMensaje("Comprador "+  identificador +" en el estante");
        estante.sumarUno();
        try {   
            sleep(tiempoAtencion);
        } catch (InterruptedException ex) {
            Logger.getLogger(HerramientasDependiente.class.getName()).log(Level.SEVERE, null, ex);
        }
        colaCaja.meterACola(this);
        estante.restaUno();
        log.enviaMensaje("Comprador " + identificador +" va a la caja");
    }
    
    public void run(){
       irAEstante();
    }
    
    public float calcularTiempoTotal(){
        float total = (float) (tiempoSalida - tiempoInicio)/1000;
        //log.enviaMensaje("El tiempo total del comprador " + getIdentificador() + " es "+ Float.toString(total));
        return total;
    }
    
    public float calcularTiempoDesdeEntrada(){
        float total = (float) (tiempoSalida - tiempoEntrada)/1000;
        //log.enviaMensaje("El tiempo desde la entrada del comprador " + getIdentificador() + " es "+ Float.toString(total));
        return total;
    }
    
}
