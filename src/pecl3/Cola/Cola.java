/*
 * Clase cola, completamente thread-safe.
 * Se usará para la cola de las cajas y para 
 * la de pescadero, estantes y carnicero.
 */
package pecl3.Cola;

import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JTextField;
import pecl3.comprador.Comprador;

/**
 *
 * @author peta_zetas
 */
public class Cola {
    
    private ArrayList<Comprador> cola;
    private JTextField tf;

    public Cola(JTextField tf){
        cola=new ArrayList<Comprador>();
        this.tf=tf;
    }
    
    public synchronized void meterACola(Comprador t){
        cola.add(t);
        imprimir();
    }
    
    public synchronized Comprador getPrimero(){
        Comprador a = cola.remove(0);
        imprimir();
        return a;        
    }
    public synchronized ArrayList<Comprador> getCola(){
        return cola;
    }
    
    public synchronized boolean estaVacía(){
        
        //System.out.println(Arrays.toString(cola.toArray()));
        return (cola.isEmpty());
    }
    
    public synchronized void imprimir(){
        String contenido="";
        for(int i=0; i<cola.size(); i++){
           contenido=contenido+cola.get(i).getIdentificador()+" ";
        }
        tf.setText(contenido);
    }
    
}