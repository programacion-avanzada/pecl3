/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pecl3.Estantes;

import javax.swing.JTextField;
import log.Log;

/**
 *
 * @author peta_zetas
 */
public class Estante{

    private JTextField text = new JTextField();
    private int contador;
    private Log log;
    
    public Estante(JTextField text, Log log) {
       this.text = text;
       this.contador = 0;
       this.log = log;
    }
   
    public synchronized void sumarUno(){
        contador  ++;
        imprimir();
    }
    
    public synchronized void restaUno(){
        contador --;
        imprimir();

    }
    
    public void imprimir(){
        text.setText(String.valueOf(contador));
        log.enviaMensaje("Hay "+ contador + " compradores");

    }
    
}
