/*
 * Esta clase escribe en un fichero, al final, algo
 * Las demás clases llaman a esta para escribir
 * Esta clase es Thread-safe
 */
package log;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalTime;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 
 * @author peta_zetas
 */
public class Log extends Thread{
    
    private Queue<String> mensajes;
    private Queue<LocalTime> horas;
    
    public Log(){
        mensajes = new ConcurrentLinkedQueue<>();
        horas = new ConcurrentLinkedDeque<>();
    }
    
    public void enviaMensaje(String mensaje){
        mensajes.offer(mensaje);
        horas.offer(LocalTime.now());
    }
       
    public synchronized void escribir(String evento, LocalTime hora){
        
        String texto = "["+hora.toString()+"]" +" <"+evento+">"+ "\n";
        try {
            Files.write(Paths.get("log.txt"), texto.getBytes(), StandardOpenOption.APPEND);             //Solo funciona en linux
        }catch (IOException e) {}
        
    }
    
    public void run(){
        String mensaje;
        LocalTime hora;
        while(true){
            if(!mensajes.isEmpty() && !horas.isEmpty()){
                mensaje = mensajes.poll();
                hora = horas.poll();
                escribir(mensaje, hora);
            }
        }
    }

    public void enviaMensaje(long duracion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

