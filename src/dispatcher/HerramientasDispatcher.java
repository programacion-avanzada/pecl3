/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dispatcher;

import pecl3.Cola.Cola;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import log.Log;
import pecl3.Estantes.Estante;
import pecl3.comprador.Calculador;
import pecl3.comprador.Comprador;
import pecl3.comprador.HerramientasGenerador;

/**
 *
 * @author peta_zetas
 */
public class HerramientasDispatcher{     
    
    private Cola colaCarnicero, colaPescadero, colaCaja, colaFuera;
    private Estante estante;
    private int aforo;
    private JTextField textDentro;
    private int dentro = 0;
    private boolean parado;
    private boolean finalizado;
    private Log log;
    private Calculador calculador;


    public HerramientasDispatcher(Cola colaCarnicero, Cola colaPescadero, Cola colaCaja, Cola colaFuera, Estante estante, int aforo, JTextField textDentro, Log log) {
        this.colaCarnicero = colaCarnicero;
        this.colaPescadero = colaPescadero;
        this.colaCaja = colaCaja;
        this.estante = estante;
        this.colaFuera = colaFuera;
        this.textDentro = textDentro;
        this.aforo = aforo;
        this.parado = false;
        this.finalizado = false;
        this.log = log;
        calculador = new Calculador(0, 0);
    }
    
    //Mete al comprador en uno de los 3 lugares
    public void asignaLugar(){
        
        Random rand = new Random(); 
                
        while(true) {  
            monitor();
            int libre = aforo - dentro;
            
            if(!colaFuera.estaVacía() && (libre >0)){                             //Mientras no esté vacía la cola de fuera y haya libre ningún hueco en el super
                         
                //System.out.println("Se cumple la condicion");
                int cola = rand.nextInt(3);
                Comprador comprador = colaFuera.getPrimero();
                
                if(cola == 0){
                    colaCarnicero.meterACola(comprador);                                //Se mete en la cola del carnicero
                    log.enviaMensaje("Comprador "+ comprador.getIdentificador() + " ha pasado a la cola del Carnicero");
                }  
                
                else if(cola == 1){     
                    colaPescadero.meterACola(comprador);                                //Se mete en la cola del pescadero
                    log.enviaMensaje("Comprador "+ comprador.getIdentificador() + " ha pasado a la cola del Pescadero");

                }          
                
                else if(cola == 2){
                    //estante.visitarEstante(colaCaja, comprador);                        //Se va directamente al estante  SE QUEDA ATASCADO AQUI
                    comprador.start();
                }            
                
                //System.out.println("Metido");
                dentro++;
                calculador.entraUno();
                log.enviaMensaje("Han entrado un total de "+ calculador.getNumeroEntrantes() + " compradores");

                imprimir();
            }
            else if (colaFuera.estaVacía()){
                //System.out.println("No hay nadie fuera");
            }
            else if(!(libre>0)){
                //System.out.println("Aforo completo");
            }
        }
    }
        
 
    
    public void adios(){
        dentro = dentro -1;
        imprimir();
    }
    
    public void imprimir(){
        textDentro.setText(String.valueOf(dentro));
    }

    
    public synchronized void monitor(){
        
        while(parado){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(HerramientasGenerador.class.getName()).log(Level.SEVERE, null, ex);   //

            }
        }
    }
    
    public void parar(){
        parado=true;
        log.enviaMensaje("Dispatcher parado");
    }
    
    
    public void finalizar(){
        finalizado = true;
        log.enviaMensaje("Dispatcher finalizado");
    }
    public synchronized void reanudar(){
        parado = false;
        notifyAll();
    }
    
}
