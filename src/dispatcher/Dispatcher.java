/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dispatcher;

/**
 *
 * @author peta_zetas
 */
public class Dispatcher extends Thread{
    
    private HerramientasDispatcher herramientasDispatcher;

    public Dispatcher(HerramientasDispatcher herramientasDispatcher) {
        this.herramientasDispatcher = herramientasDispatcher;
    }
    
    public void run(){
        herramientasDispatcher.asignaLugar();
    }
}
